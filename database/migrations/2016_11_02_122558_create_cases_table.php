<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case__descs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ar_title')->nullable();
            $table->string('en_title')->nullable();
            $table->text('images')->nullable();
            $table->text('ar_content')->nullable();
            $table->text('en_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case__descs');
    }
}
