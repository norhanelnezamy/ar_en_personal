<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('settings', function (Blueprint $table) {
          $table->increments('id');
          $table->text('ar_about')->nullable();
          $table->text('en_about')->nullable();
          $table->string('ar_name')->nullable();
          $table->string('en_name')->nullable();
          $table->string('ar_address')->nullable();
          $table->string('en_address')->nullable();
          $table->string('phone')->nullable();
          $table->string('email')->nullable();
          $table->string('image')->nullable();
          $table->string('ar_special')->nullable();
          $table->string('en_special')->nullable();
          $table->string('ar_degree')->nullable();
          $table->string('ar_bio')->nullable();
          $table->string('en_degree')->nullable();
          $table->string('en_bio')->nullable();
          $table->string('facebook')->nullable();
          $table->string('twitter')->nullable();
          $table->string('youtube')->nullable();
          $table->string('video')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
