<?php

Auth::routes();
Route::group(['prefix' => '/admin'], function () {
  Route::group(['middleware' => 'auth'] , function () {
    Route::post('/upload' , 'SettingController@upload');
    Route::get('/setting/ar_about' , 'SettingController@getArAbout');
    Route::get('/setting/en_about' , 'SettingController@getEnAbout');
    Route::get('/setting/personal' , 'SettingController@getPersonal');
    Route::post('/setting/ar_about' , 'SettingController@postArAbout');
    Route::post('/setting/en_about' , 'SettingController@postEnAbout');
    Route::post('/setting/personal' , 'SettingController@postPersonal');
    Route::resource('/case' , 'CaseController');
    Route::resource('/gallery' , 'GalleryController');
    Route::resource('/user' , 'UserController');
    Route::resource('/articel' , 'ArticelController');
    Route::resource('/news' , 'NewsController');
    Route::resource('/slider' , 'SliderController');
    Route::resource('/time' , 'TimeController');
    Route::resource('/' , 'IndexController');
  });
});
Route::get('/contact' , 'HomeController@getContact');
Route::get('/about' , 'HomeController@getAbout');
Route::get('/gallery' , 'HomeController@getgallery');
Route::get('/video' , 'HomeController@getVideo');
Route::get('/articel' , 'HomeController@getArticel');
Route::get('/news' , 'HomeController@getNews');
Route::get('/case' , 'HomeController@getCase');
Route::get('/case/{id}' , 'HomeController@showCase');
Route::get('/articel/{id}' , 'HomeController@showArticel');
Route::get('/news/{id}' , 'HomeController@showNews');
Route::resource('/' , 'HomeController');
