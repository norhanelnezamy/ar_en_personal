<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
  public static function personal($request){
    $setting = Setting::first();
    $setting->ar_name = $request->ar_name;
    $setting->ar_bio = $request->ar_bio;
    $setting->phone = $request->phone;
    $setting->email = $request->email;
    $setting->facebook = $request->facebook;
    $setting->twitter = $request->twitter;
    $setting->youtube = $request->youtube;
    $setting->video = $request->video;
    $setting->ar_special = $request->ar_special;
    $setting->ar_degree = $request->ar_degree;
    $setting->ar_address = $request->ar_address;
    $setting->en_address = $request->en_address;
    $setting->en_name = $request->en_name;
    $setting->en_bio = $request->en_bio;
    $setting->en_special = $request->en_special;
    $setting->en_degree = $request->en_degree;
    if (!empty($request->image)) {
      $setting->image = upload($request->image , 'images');
    }
    $setting->save();
  }
  public static function enAbout($request){
    $setting = Setting::first();
    $setting->en_about = $request->en_content;
    $setting->save();
  }
  public static function arAbout($request){
    $setting = Setting::first();
    $setting->ar_about = $request->ar_content;
    $setting->save();
  }
}
