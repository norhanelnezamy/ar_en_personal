<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
  public static function add($request){
    $news = new News;
    $news->ar_title = $request->ar_title;
    $news->ar_content = $request->ar_content;
    $news->en_title = $request->en_title;
    $news->en_content = $request->en_content;
    $news->image = upload($request->image,'images');
    $news->save();
  }
  public static function edit($request , $id){
    $news = News::findOrFail($id);
    $news->ar_title = $request->ar_title;
    $news->ar_content = $request->ar_content;
    $news->en_title = $request->en_title;
    $news->en_content = $request->en_content;
    if (!empty($request->image)) {
      $news->image = upload($request->image,'images');
    }
    $news->save();
  }
}
