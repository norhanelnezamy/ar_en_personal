<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Case_Desc extends Model
{
  public static function add($request){
    $case = new Case_Desc;
    $case->ar_title = $request->ar_title;
    $case->ar_content = $request->ar_content;
    $case->en_title = $request->en_title;
    $case->en_content = $request->en_content;
    $case->images = $request->image;
    $case->save();
  }
  public static function edit($request , $id){
    $case = Case_Desc::findOrFail($id);
    $case->ar_title = $request->ar_title;
    $case->ar_content = $request->ar_content;
    $case->en_title = $request->en_title;
    $case->en_content = $request->en_content;
    if (!empty($request->image)) {
      $case->images = $request->image;
    }
    $case->save();
  }
}
