<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    public static function add($request){
      $time = new Time;
      $time->day = $request->day;
      $time->time = $request->time;
      $time->save();
    }

    public static function edit($request, $id){
      $time = Time::findOrFail($id);
      $time->day = $request->day;
      $time->time = $request->time;
      $time->save();
    }
}
