<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ArticelRequest;
use App\Case_Desc;

class CaseController extends Controller
{
  // get only 5 cases
  public function index()
  {
    $cases = Case_Desc::paginate(5);
    return view('admin.case.index',compact('cases'));
  }
  // return new case
  public function create()
  {
    return view('admin.case.create');
  }
  //store new case
  public function store(ArticelRequest $request)
  {
    foreach ($request->image as $key => $one) {
      $images[$key] = upload($one,'images');
    }
    $request->image = implode("|",$images);
    Case_Desc::add($request);
    return redirect()->back();
  }
  // show case by id
  public function show($id)
  {
    $case = Case_Desc::findOrFail($id);
    return view('admin.case.show',compact('case'));
  }
  //return update view with case
  public function edit($id)
  {
    $case = Case_Desc::findOrFail($id);
    return view('admin.case.edit',compact('case'));
  }
  //update case
  public function update(ArticelRequest $request, $id)
  {
    Case_Desc::edit($request ,$id);
    return redirect()->back();
  }
  //delete case
  public function destroy($id)
  {
    $case = Case_Desc::findOrFail($id)->delete();
    return redirect()->back();
  }
}
