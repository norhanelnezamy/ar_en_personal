<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Setting;

class SettingController extends Controller
{
  public function upload(Request $request){
    return  upload($request->image , 'images');
  }
  public function getArAbout(){
    $setting = Setting::first();
    return view('admin.ar_about',compact('setting'));
  }
  public function getEnAbout(){
    $setting = Setting::first();
    return view('admin.en_about',compact('setting'));
  }
  public function getPersonal(){
    $setting = Setting::first();
    return view('admin.personal',compact('setting'));
  }
  public function postArAbout(Request $request){
    Setting::arAbout($request);
    return redirect()->back();
  }
  public function postEnAbout(Request $request){
    Setting::enAbout($request);
    return redirect()->back();
  }
  public function postPersonal(Request $request){
    Setting::personal($request);
    return redirect()->back();
  }
}
