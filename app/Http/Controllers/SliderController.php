<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Slider;

class SliderController extends Controller
{
  // get only 5 sliders
  public function index()
  {
    $sliders = Slider::paginate(5);
    return view('admin.slider.index',compact('sliders'));
  }
  // return new slider view
  public function create()
  {
    return view('admin.slider.create');
  }
  //store new slider
  public function store(Request $request)
  {
    Slider::add($request);
    return redirect()->back();
  }
  // show slider image ,title and text by id
  public function show($id)
  {
    $slider = Slider::findOrFail($id);
    return view('admin.slider.show',compact('slider'));
  }
  //return update view with slider
  public function edit($id)
  {
    $slider = Slider::findOrFail($id);
    return view('admin.slider.edit',compact('slider'));
  }
  //update slider
  public function update(Request $request, $id)
  {
    Slider::edit($request ,$id);
    return redirect()->back();
  }
  //delete slider
  public function destroy($id)
  {
    $slider = slider::findOrFail($id)->delete();
    return redirect()->back();
  }
}
