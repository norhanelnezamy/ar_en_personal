<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\VideoRequest;
use App\Video;

class VideoController extends Controller
{
  // get only 5 videos
  public function index()
  {
    $videos = Video::paginate(5);
    return view('admin.video.index',compact('videos'));
  }
  // return new video
  public function create()
  {
    return view('admin.video.create');
  }
  //store new video
  public function store(VideoRequest $request)
  {
    Video::add($request);
    return redirect()->back();
  }
  // show video by id
  public function show($id)
  {
    $video = Video::findOrFail($id);
    return view('admin.video.show',compact('video'));
  }
  //return update view with video
  public function edit($id)
  {
    $video = Video::findOrFail($id);
    return view('admin.video.edit',compact('video'));
  }
  //update video
  public function update(VideoRequest $request, $id)
  {
    Video::edit($request ,$id);
    return redirect()->back();
  }
  //delete video
  public function destroy($id)
  {
    $video = Video::findOrFail($id)->delete();
    return redirect()->back();
  }
}
