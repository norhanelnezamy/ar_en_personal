<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ArticelRequest;
use App\Articel;

class ArticelController extends Controller
{
  // get only 5 articels
  public function index()
  {
    $articels = Articel::paginate(5);
    return view('admin.articel.index',compact('articels'));
  }
  // return new articel
  public function create()
  {
    return view('admin.articel.create');
  }
  //store new articel
  public function store(ArticelRequest $request)
  {
    Articel::add($request);
    return redirect()->back();
  }
  // show articel by id
  public function show($id)
  {
    $articel = Articel::findOrFail($id);
    return view('admin.articel.show',compact('articel'));
  }
  //return update view with articel
  public function edit($id)
  {
    $articel = Articel::findOrFail($id);
    return view('admin.articel.edit',compact('articel'));
  }
  //update articel
  public function update(ArticelRequest $request, $id)
  {
    Articel::edit($request ,$id);
    return redirect()->back();
  }
  //delete articel
  public function destroy($id)
  {
    $articel = Articel::findOrFail($id)->delete();
    return redirect()->back();
  }
}
