<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Gallery;
use App\News;
use App\Articel;
use App\Video;
use App\Setting;
use App\Case_Desc;
use App\Time;

class HomeController extends Controller
{
    public function index()
    {
      $slider = Slider::all();
      $gallery = Gallery::orderBy('created_at','ASC')->limit(6)->get();
      $articels = Articel::orderBy('created_at','ASC')->limit(3)->get();
      $cases = Case_Desc::orderBy('created_at','ASC')->limit(3)->get();
      $setting = Setting::first();
      $news = News::orderBy('created_at','ASC')->limit(3)->get();
      $times = Time::all();
      $page = 'home';
      return view('home',compact('slider','gallery','news','cases','articels','news','articels','video','setting','page','times'));
    }
    public function getGallery(){
      $page = 'gallery';
      $setting = Setting::first();
      $gallery = Gallery::orderBy('created_at','ASC')->paginate(10);
      return view('gallery', compact('gallery','setting','page'));
    }
    public function getArticel(){
      $page = 'articel';
      $setting = Setting::first();
      $articels = Articel::orderBy('created_at','ASC')->paginate(10);
      return view('articel', compact('articels','setting','page'));
    }
    public function getNews(){
      $page = 'news';
      $setting = Setting::first();
      $news = News::orderBy('created_at','ASC')->paginate(10);
      return view('news', compact('news','setting','page'));
    }
    public function getVideo(){
      $page = 'video';
      $setting = Setting::first();
      $videos = Video::orderBy('created_at','ASC')->paginate(10);
      return view('video', compact('videos','setting','page'));
    }
    public function getCase(){
      $page = 'case';
      $setting = Setting::first();
      $cases = Case_Desc::orderBy('created_at','ASC')->paginate(10);
      return view('case', compact('cases','setting','page'));
    }
    public function showArticel($id){
      $page = 'articel';
      $setting = Setting::first();
      $articel = Articel::findOrFail($id);
      return view('show_articel', compact('articel','setting','page'));
    }
    public function showNews($id){
      $page = 'news';
      $setting = Setting::first();
      $news = News::findOrFail($id);
      return view('show_news', compact('news','setting','page'));
    }
    public function showCase($id){
      $page = 'case';
      $setting = Setting::first();
      $case = Case_Desc::findOrFail($id);
      return view('show_case', compact('case','setting','page'));
    }
    public function getAbout(){
      $page = 'about';
      $setting = Setting::first();
      $times = Time::all();
      return view('about', compact('setting','page','times'));
    }
    public function getContact(){
      $page = 'contact';
      $setting = Setting::first();
      return view('contact', compact('setting','page'));
    }
}
