<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;

class UserController extends Controller
{
  // get only 5 users
  public function index()
  {
    $users = User::paginate(5);
    return view('admin.user.index',compact('users'));
  }
  // return new user view
  public function create()
  {
    return view('admin.user.create');
  }
  //store new user
  public function store(UserRequest $request)
  {
    User::add($request);
    return redirect()->back();
  }
  // show user by id
  public function show($id)
  {
    $user = User::findOrFail($id);
    return view('admin.user.show',compact('user'));
  }
  //return update view with user
  public function edit($id)
  {
    $user = User::findOrFail($id);
    return view('admin.user.edit',compact('user'));
  }
  //update user
  public function update(UpdateUserRequest $request, $id)
  {
    User::edit($request ,$id);
    return redirect()->back();
  }
  //delete user
  public function destroy($id)
  {
    $user = User::findOrFail($id)->delete();
    return redirect()->back();
  }
}
