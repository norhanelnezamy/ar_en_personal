<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ArticelRequest;
use App\News;

class NewsController extends Controller
{
  // get only 5 news
  public function index()
  {
    $news = News::paginate(5);
    return view('admin.news.index',compact('news'));
  }
  // return new new view
  public function create()
  {
    return view('admin.news.create');
  }
  //store new news
  public function store(ArticelRequest $request)
  {
    News::add($request);
    return redirect()->back();
  }
  // show news by id
  public function show($id)
  {
    $news = News::findOrFail($id);
    return view('admin.news.show',compact('news'));
  }
  //return update view with news
  public function edit($id)
  {
    $news = News::findOrFail($id);
    return view('admin.news.edit',compact('news'));
  }
  //update news
  public function update(ArticelRequest $request, $id)
  {
    News::edit($request ,$id);
    return redirect()->back();
  }
  //delete news
  public function destroy($id)
  {
    $news = News::findOrFail($id)->delete();
    return redirect()->back();
  }
}
