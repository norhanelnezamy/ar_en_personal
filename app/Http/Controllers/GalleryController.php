<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Gallery;

class GalleryController extends Controller
{
  // get only 5 galaries
  public function index()
  {
    $galaries = Gallery::paginate(5);
    return view('admin.gallery.index',compact('galaries'));
  }
  // return new gallery
  public function create()
  {
    return view('admin.gallery.create');
  }
  //store new gallery
  public function store(Request $request)
  {
    Gallery::add($request);
    return redirect()->back();
  }
  // show gallery by id
  public function show($id)
  {
    $gallery = Gallery::findOrFail($id);
    return view('admin.gallery.show',compact('gallery'));
  }
  //return update view with gallery
  public function edit($id)
  {
    $gallery = Gallery::findOrFail($id);
    return view('admin.gallery.edit',compact('gallery'));
  }
  //update gallery
  public function update(Request $request, $id)
  {
    Gallery::edit($request ,$id);
    return redirect()->back();
  }
  //delete gallery
  public function destroy($id)
  {
    $gallery = Gallery::findOrFail($id)->delete();
    return redirect()->back();
  }
}
