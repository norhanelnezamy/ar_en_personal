<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
  public static function add($request){
    $slider = new Slider;
    $slider->ar_title = $request->ar_title;
    $slider->ar_text = $request->ar_text;
    $slider->en_title = $request->en_title;
    $slider->en_text = $request->en_text;
    $slider->image = upload($request->image,'images');
    $slider->save();
  }
  public static function edit($request , $id){
    $slider = Slider::findOrFail($id);
    $slider->ar_title = $request->ar_title;
    $slider->ar_text = $request->ar_text;
    $slider->en_title = $request->en_title;
    $slider->en_text = $request->en_text;
    if (!empty($request->image)) {
      $slider->image = upload($request->image,'images');
    }
    $slider->save();
  }
}
