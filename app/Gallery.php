<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
  public static function add($request){
    $gallery = new Gallery;
    $gallery->ar_title = $request->ar_title;
    $gallery->en_title = $request->en_title;
    $gallery->image = upload($request->image,'images');
    $gallery->save();
  }
  public static function edit($request , $id){
    $gallery = Gallery::findOrFail($id);
    $gallery->ar_title = $request->ar_title;
    $gallery->en_title = $request->en_title;
    if (!empty($request->image)) {
      $gallery->image = upload($request->image,'images');
    }
    $gallery->save();
  }
}
