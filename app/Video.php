<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
  public static function add($request){
    $video = new Video;
    $video->ar_title = $request->ar_title;
    $video->ar_desc = $request->ar_desc;
    $video->en_title = $request->en_title;
    $video->en_desc = $request->en_dessc;
    $video->url = $request->url;
    $video->save();
  }
  public static function edit($request , $id){
    $video = Video::findOrFail($id);
    $video->ar_title = $request->ar_title;
    $video->ar_desc = $request->ar_desc;
    $video->en_title = $request->en_title;
    $video->en_desc = $request->en_desc;
    $video->url = $request->url;
    $video->save();
  }
}
