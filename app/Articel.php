<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articel extends Model
{
  public static function add($request){
    $articel = new Articel;
    $articel->ar_title = $request->ar_title;
    $articel->ar_content = $request->ar_content;
    $articel->en_title = $request->en_title;
    $articel->en_content = $request->en_content;
    $articel->image = upload($request->image,'images');
    $articel->save();
  }
  public static function edit($request , $id){
    $articel = Articel::findOrFail($id);
    $articel->ar_title = $request->ar_title;
    $articel->ar_content = $request->ar_content;
    $articel->en_title = $request->en_title;
    $articel->en_content = $request->en_content;
    if (!empty($request->image)) {
      $articel->image = upload($request->image,'images');
    }
    $articel->save();
  }
}
