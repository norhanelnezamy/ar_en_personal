@extends('layouts.app')
  @section('slider')
    @include('layouts.slider')
  @endsection
  @section('about')
    @include('layouts.about')
  @endsection
  @section('content')
    <!-- Section: Gallery -->
    <section id="gallery" class="bg-lightest">
      <div class="container pb-30 pt-50">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <h2 class="title text-uppercase"><span class="text-black font-weight-300">معرض الصور </span> </h2><p class="text-uppercase letter-space-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, tenetur.</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <!-- Portfolio Gallery Grid -->
              <div id="grid" class="portfolio-gallery grid-6 clearfix">
                <!-- Portfolio Item Start -->
                @foreach ($gallery as $key => $item)
                  <div class="portfolio-item photography">
                    <div class="thumb">
                      <img class="img-fullwidth" src="{{asset('images/'.$item->image)}}" alt="project"  style="width:189px;height:189px;">
                      <div class="overlay-shade"></div>
                      <div class="text-holder text-center">
                        <h5 class="title">{{$item->ar_title}}</h5>
                      </div>
                      <div class="icons-holder">
                        <div class="icons-holder-inner">
                          <div class="font-icons icon-sm icon-dark icon-circled icon-tc">
                            <a data-lightbox="image" href="{{asset('images/'.$item->image)}}"><i class="fa fa-plus"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
                <!-- Portfolio Item End -->

              </div>
            </div>
          </div>
        </div>
        <div class="portfolio-filter text-center">
          <a href="{{asset('/gallery')}}" class="active" data-filter="*">المزيد </a>
        </div>
      </div>
    </section>
    <!-- Section: Gallery -->
    <section id="gallery" class="bg-lightest">
      <div class="container pb-30 pt-50">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <h2 class="title text-uppercase"><span class="text-black font-weight-300">فيديو </span></h2><p class="text-uppercase letter-space-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, tenetur.</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <iframe height="350" src="{{$setting->video}}"></iframe>
            </div>
          </div>
        </div>

      </div>
    </section>
    <!-- Section: News -->
    <section id="news">
      <div class="container pb-30 pt-50">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <h2 class="title text-uppercase"><span class="text-black font-weight-300">اخر الاخبار </span></h2>
              <p class="text-uppercase letter-space-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, tenetur.</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            @foreach ($news as $key => $item)
              <div class="col-sm-12 col-md-4">
                <article class="post clearfix mb-30">
                  <div class="entry-header">
                    <div class="post-thumb thumb">
                      <img class="img-responsive img-fullwidth" alt="" src="{{asset('images/'.$item->image)}}" style="width:360px;height:260px;">
                    </div>
                  </div>
                  <div class="entry-content p-20">
                    <h4 class="entry-title text-white text-uppercase"><a href="#">{{$item->ar_title}}</a></h4>
                    <p class="pt-5"><?php echo $item->ar_content;?> <a href="{{asset('news/'.$item->id)}}" class="text-tc"><br><strong>Read More...</strong></a></p>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
            @endforeach
          </div>
        </div>
        <div class="portfolio-filter text-center">
          <a href="{{asset('news')}}" class="active" data-filter="*">المزيد </a>
        </div>
      </div>
    </section>
    <!-- Section: Articel -->
    <section id="news">
      <div class="container pb-30 pt-50">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <h2 class="title text-uppercase"><span class="text-black font-weight-300"> اخر المقالات  </span></h2>
              <p class="text-uppercase letter-space-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, tenetur.</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            @foreach ($articels as $key => $item)
              <div class="col-sm-12 col-md-4">
                <article class="post clearfix mb-30">
                  <div class="entry-header">
                    <div class="post-thumb thumb">
                      <img class="img-responsive img-fullwidth" alt="" src="{{asset('images/'.$item->image)}}"  style="width:360px;height:340px;">
                    </div>
                  </div>
                  <div class="entry-content p-20">
                    <h4 class="entry-title text-white text-uppercase"><a href="#">{{$item->ar_title}}</a></h4>
                    <p class="pt-5"><?php echo $item->ar_content;?> <a href="{{asset('articel/'.$item->id)}}" class="text-tc"><br><strong>Read More...</strong></a></p>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
            @endforeach
          </div>
        </div>
        <div class="portfolio-filter text-center">
          <a href="{{asset('articel')}}" class="active" data-filter="*">المزيد </a>
        </div>
      </div>
    </section>
    <!-- Divider: Contact -->
    <section id="contact" class="divider">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <h2 class="title text-uppercase"><span class="text-black font-weight-300">اتصل بنا </span>  </h2>
              <p class="text-uppercase letter-space-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, tenetur.</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row pt-30">
            <div class="col-md-5">

              <!-- Google Map -->
              <div
                data-address="121 King Street, Melbourne Victoria 3000 Australia"
                data-popupstring-id="#popupstring1"
                class="map-canvas autoload-map"
                data-mapstyle="style2"
                data-height="500"
                data-latlng="-37.817314,144.955431"
                data-title="sample title"
                data-zoom="12"
                data-marker="images/map-icon-blue.png">
              </div>
              <div class="map-popupstring hidden" id="popupstring1">
                <div class="text-center">
                  <h3>Medinova Office</h3>
                  <p>121 King Street, Melbourne Victoria 3000 Australia</p>
                </div>
              </div>

            </div>
            <div class="col-md-7">
              <!-- Contact Form -->
              <form id="contact_form" name="contact_form" class="" action="includes/sendmail.php" method="post">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="form_name">الاسم <small>*</small></label>
                      <input id="form_name" name="form_name" class="form-control" type="text" placeholder="Enter Name" required="">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="form_email">البريدالالكترونى <small>*</small></label>
                      <input id="form_email" name="form_email" class="form-control required email" type="email" placeholder="ادخل الاسم ">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="form_name">الموضوع <small>*</small></label>
                      <input id="form_subject" name="form_subject" class="form-control required" type="text" placeholder="ادخل الموضوع ">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="form_phone">رقم التليفون </label>
                      <input id="form_phone" name="form_phone" class="form-control" type="text" placeholder="ادخل رقم التليفون ">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="form_name">نص الرساله </label>
                  <textarea id="form_message" name="form_message" class="form-control required" rows="5" placeholder="ادخل نص الرساله "></textarea>
                </div>
                <div class="form-group">
                  <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />
                  <button type="submit" class="btn btn-dark btn-tc btn-flat" data-loading-text="Please wait...">ارسال </button>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
  @endsection
