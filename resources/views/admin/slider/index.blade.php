@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">sliders</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" >
    <div class="row col-md-8 well " style="margin-left:1px;">
      <h4><a href="{{ asset('/admin/slider/create') }}"> New slider</a> </h4>
      <hr>
      <table class="table table-bordered">
          <thead>
          <tr>
              <th>#</th>
              <th>عنوان - عربي</th>
              <th>عنوان - انجليزى</th>
              <th>نص - عربي</th>
              <th>نص - انجليزى</th>
              <th>صوره </th>
              <th>التحكم</th>
          </tr>
          </thead>
          <tbody>
          @if(sizeof($sliders) > 0)
              <?php $count = 0?>
              @foreach($sliders as $slider)
                  <?php $count++?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$slider->ar_title}}</td>
                    <td>{{$slider->en_title}}</td>
                    <td>{{$slider->ar_text}}</td>
                    <td>{{$slider->en_text}}</td>
                    <td><img src="{{ asset('/images/'.$slider->image) }}" style="width:70px; height:70px"></td>
                    <td>{{$slider->created_at}}</td>
                    <td>
                      <div class="form-inline">
                        <form action="{{ url('admin/slider/'. $slider->id.'/edit') }}" method="get">
                          <button class="glyphicon glyphicon-pencil" title="update"></button>
                        </form>
                        <form action="{{ url('admin/slider/'. $slider->id ) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field("DELETE") }}
                          <button class="glyphicon glyphicon-remove" title="delete"></button>
                        </form>
                      </div>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
      </table>
      {{ $sliders->links() }}
    </div>
    </section>
  @endsection
