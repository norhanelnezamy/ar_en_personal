@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">videos</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" >
    <div class="row col-md-8 well " style="margin-left:1px;">
      <h4><a href="{{ asset('/admin/video/create') }}"> New video</a> </h4>
      <hr>
      <table class="table table-bordered">
          <thead>
          <tr>
              <th>#</th>
              <th>عنوان - عربي</th>
              <th>عنوان - انجليزى</th>
              <th>وصف - عربي</th>
              <th>وصف - انجليزى</th>
              <th>التحكم</th>
          </tr>
          </thead>
          <tbody>
          @if(sizeof($videos) > 0)
              <?php $count = 0?>
              @foreach($videos as $video)
                  <?php $count++?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$video->ar_title}}</td>
                    <td>{{$video->en_title}}</td>
                    <td>{{$video->ar_desc}}</td>
                    <td>{{$video->en_desc}}</td>
                    <td>{{$video->created_at}}</td>
                    <td>
                      <div class="form-inline">
                        <a class="glyphicon glyphicon-eye-open" title="show" href="{{asset('/admin/video/'.$video->id)}}"></a>
                        <form action="{{ url('admin/video/'. $video->id.'/edit') }}" method="get">
                          <button class="glyphicon glyphicon-pencil" title="update"></button>
                        </form>
                        <form action="{{ url('admin/video/'. $video->id ) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field("DELETE") }}
                          <button class="glyphicon glyphicon-remove" title="delete"></button>
                        </form>
                      </div>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
      </table>
      {{ $videos->links() }}
    </div>
    </section>
  @endsection
