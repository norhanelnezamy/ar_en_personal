@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">add video</li>
      </ol>
    </section>
    <!-- Main content -->

    <section class="content">
    <div class="row col-md-8 well " style="margin:2%;">
      <form method="POST" action="{{ asset('/admin/video') }}" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <h3>اضافه فيديو </h3>
          <div class="col-sm-6">
            <div class="form-group">
              <label>رابط الفيديو</label>
              <input class="form-control" type="url" name="url" value="{{old('url')}}" placeholder="enter url"required>
              @if ($errors->has('url'))
               <p style="color:red">{{ $errors->first('url') }}</p>
              @endif
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>عنوان - عربي</label>
              <input class="form-control" type="text" name="ar_title" value="{{old('ar_title')}}" placeholder="enter arabic title"required>
              @if ($errors->has('ar_title'))
               <p style="color:red">{{ $errors->first('ar_title') }}</p>
              @endif
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>عنوان - انجليزى</label>
              <input class="form-control" type="text" name="en_title" value="{{old('en_title')}}" placeholder="enter english title" required>
              @if ($errors->has('en_title'))
               <p style="color:red">{{ $errors->first('en_title') }}</p>
              @endif
            </div>
          </div>
            <div class="form-group">
              <label>وصف - عربي </label>
              <textarea class="form-control" name="ar_desc" placeholder="enter arabic description" required>{{old('ar_desc')}}</textarea>
              @if ($errors->has('ar_desc'))
               <p style="color:red">{{ $errors->first('ar_desc') }}</p>
              @endif
            </div>
            <div class="form-group">
              <label>وصف - انجليزى</label>
              <textarea class="form-control" name="en_desc" placeholder="enter english description" required>{{old('en_desc')}}</textarea>
              @if ($errors->has('en_desc'))
               <p style="color:red">{{ $errors->first('en_desc') }}</p>
              @endif
            </div>
          <button type="submit" class="btn btn-primary" style="width:100%;">اضافه </button>
      </form>
    </div>
    </section>
  @endsection
