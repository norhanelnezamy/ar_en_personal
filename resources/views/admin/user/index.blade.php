@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">user</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" >
    <div class="row col-md-7 well " style="margin-left:1px;">
      <h4><a href="{{ asset('/admin/user/create') }}"> New user</a> </h4>
      <hr>
      <table class="table table-bordered">
          <thead>
          <tr>
              <th>#</th>
              <th>name</th>
              <th>email</th>
              <th>actions</th>
          </tr>
          </thead>
          <tbody>
          @if(sizeof($users) > 0)
              <?php $count = 0?>
              @foreach($users as $user)
                  <?php $count++?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                      <form action="{{ url('admin/user/'. $user->id.'/edit') }}" method="get">
                        <button class="glyphicon glyphicon-pencil" title="update"></button>
                      </form>
                      <form action="{{ url('admin/user/'. $user->id ) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field("DELETE") }}
                        <button class="glyphicon glyphicon-remove" title="delete"></button>
                      </form>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
      </table>
      {{ $users->links() }}
    </div>
    </section>
  @endsection
