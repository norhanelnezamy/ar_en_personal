@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">add user</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row col-md-6 well " style="margin-left:20%;margin-top:2%;">
      <form method="POST" action="{{ asset('admin/user') }}">
          {!! csrf_field() !!}
          <h3>Add user</h3>
          <hr>
          <div class="form-group">
            <label>Name</label>
            <input class="form-control" type="text" name="name" value="{{old('name')}}" placeholder="enter person name" required>
            @if ($errors->has('name'))
             <p style="color:red">{{ $errors->first('name') }}</p>
            @endif
          </div>
          <div class="form-group">
            <label>Email</label>
            <input class="form-control" type="email" name="email" value="{{old('email')}}" placeholder="enter email" required>
            @if ($errors->has('email'))
             <p style="color:red">{{ $errors->first('email') }}</p>
            @endif
          </div>
          <div class="form-group">
            <label>PASSWORD</label>
            <input class="form-control" type="password" name="password" value="{{old('password')}}" placeholder="enter password" required>
            @if ($errors->has('password'))
            <p style="color:red">{{ $errors->first('password') }}</p>
            @endif
          </div>
          <div class="form-group">
            <label>CONFIRM PASSWORD</label>
            <input class="form-control" type="password" name="password_confirmation" value="{{old('password_confirmation')}}" placeholder="confirme password" required>
            @if ($errors->has('password_confirmation'))
            <p style="color:red">{{ $errors->first('password_confirmation') }}</p>
            @endif
          </div>
          <button type="submit" class="btn btn-primary" style="width:100%;">create</button>
      </form>
    </div>
    </section>
  @endsection
