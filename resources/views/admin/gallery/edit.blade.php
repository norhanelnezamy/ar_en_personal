@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">add gallery</li>
      </ol>
    </section>
    <!-- Main content -->

    <section class="content">
    <div class="row col-md-8 well " style="margin:2%;">
      <form method="POST" action="{{ asset('/admin/gallery/'.$gallery->id) }}" enctype="multipart/form-data">
          {!! csrf_field() !!}
          {{method_field("PUT")}}
          <h3>تعديل </h3>
          <hr>
          <div class="col-sm-6 image">
            <div class="form-group">
              <span><b> صوره </b></span><br>
              <input type='file' name="image" id="image" />
            </div>
          </div>
          <div class="col-sm-6 image">
            <div class="form-group">
              <img id="blah" src="{{asset('images/'.$gallery->image)}}" alt="your image" style="width:150px;height:150px;"/>
            </div>
          </div>
          @if ($errors->has('image'))
           <p style="color:red">{{ $errors->first('image') }}</p>
          @endif
          <div class="col-sm-6">
            <div class="form-group">
              <label>عنوان - عربي</label>
              <input class="form-control" type="text" name="ar_title" value="{{old('ar_title',$gallery->ar_title)}}" placeholder="enter arabic title"required>
              @if ($errors->has('ar_title'))
               <p style="color:red">{{ $errors->first('ar_title') }}</p>
              @endif
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>عنوان - انجليزى</label>
              <input class="form-control" type="text" name="en_title" value="{{old('en_title',$gallery->en_title)}}" placeholder="enter english title" required>
              @if ($errors->has('en_title'))
               <p style="color:red">{{ $errors->first('en_title') }}</p>
              @endif
            </div>
          </div>
          <button type="submit" class="btn btn-primary" style="width:100%;">تعديل</button>
      </form>
    </div>
    </section>
  @endsection
