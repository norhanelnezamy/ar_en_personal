@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">galaries</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" >
    <div class="row col-md-9 well " style="margin-left:1px;">
      <h4><a href="{{ asset('/admin/gallery/create') }}"> New gallery</a> </h4>
      <hr>
      <table class="table table-bordered">
          <thead>
          <tr>
              <th>#</th>
              <th>عنوان - عربي</th>
              <th>عنوان - انجليزى</th>
              <th>صوره </th>
              <th>التحكم </th>
          </tr>
          </thead>
          <tbody>
          @if(sizeof($galaries) > 0)
              <?php $count = 0?>
              @foreach($galaries as $gallery)
                  <?php $count++?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$gallery->ar_title}}</td>
                    <td>{{$gallery->en_title}}</td>
                    <td><img src="{{ asset('/images/'.$gallery->image) }}" style="width:70px; height:70px"></td>
                    <td>{{$gallery->created_at}}</td>
                    <td>
                        <form action="{{ url('admin/gallery/'. $gallery->id.'/edit') }}" method="get">
                          <button class="btn btn-warning glyphicon glyphicon-pencil" title="update"></button>
                        </form>
                        <form action="{{ url('admin/gallery/'. $gallery->id ) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field("DELETE") }}
                          <button class="btn btn-danger glyphicon glyphicon-remove" title="delete"></button>
                        </form>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
      </table>
      {{ $galaries->links() }}
    </div>
    </section>
  @endsection
