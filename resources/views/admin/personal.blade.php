@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/masterAdmin')}}"><i class="fa fa-dashboard"></i>dashboard</a></li>
        <li class="active">personal data</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row col-md-7 well " style="margin:2%;padding:3%;">
      <form method="post" action="{{asset('admin/setting/personal')}}" enctype="multipart/form-data">
            <div class="row">
            {{csrf_field()}}
            <div class="col-sm-6 image">
              <div class="form-group">
                <span><b>صوره</b></span><br>
                <input type='file' name="image" id="image" accept="image/*" />
              </div>
            </div>
            <div class="col-sm-6 image">
              <div class="form-group">
                <img id="blah" src="{{asset('images/'.$setting->image)}}" alt="your image" style="width:150px;height:100px;"/>
              </div>
            </div>
            <div class="form-group ">
              <span><b>الاسم - عربي</b></span><br>
              <input type="text" name="ar_name" placeholder="" value="{{old('ar_name',$setting->ar_name)}}" class="form-control" >
            </div>
            <div class="form-group ">
              <span><b>العنوان -عربي </b></span><br>
              <input type="text" name="ar_address" placeholder="" value="{{old('ar_address',$setting->ar_address)}}" class="form-control" >
            </div>
            <div class="form-group ">
              <span><b>العنوان -انجليزى </b></span><br>
              <input type="text" name="en_address" placeholder="" value="{{old('en_address',$setting->en_address)}}" class="form-control" >
            </div>
            <div class="form-group ">
              <span><b>الاسم - انجليزى</b></span><br>
              <input type="text" name="en_name" placeholder="" value="{{old('en_name',$setting->en_name)}}" class="form-control" >
            </div>
            <div class="form-group">
              <span><b>رقم التليفون </b></span><br>
              <input type="text" name="phone" placeholder="01000000000" value="{{old('phone',$setting->phone)}}" class="form-control" >
            </div>
            <div class="form-group">
              <span><b>البريد الالكترونى </b></span><br>
              <input type="email" name="email" placeholder="Mohamed...@gmail.com" value="{{old('email',$setting->email)}}" class="form-control">
            </div>
            <div class="form-group">
              <span><b>فيديو </b></span><br>
              <input type="url" pattern="https?://.+" name="video" placeholder="http://www.youtube.com/..." value="{{old('video',$setting->video)}}" class="form-control">
            </div>
            <div class="form-group">
              <span><b>رابط الفيسبوك </b></span><br>
              <input type="url" pattern="https?://.+" name="facebook" placeholder="http://www.facebook.com/Mohamed..." value="{{old('facebook',$setting->facebook)}}" class="form-control">
            </div>
            <div class="form-group">
              <span><b>رابط تويتر </b></span><br>
              <input type="url" pattern="https?://.+" name="twitter" placeholder="http://www.twitter.com/Mohamed..." value="{{old('twitter',$setting->twitter)}}" class="form-control">
            </div>
            <div class="form-group">
              <span><b>رابط قناه اليوتيوب </b></span><br>
              <input type="url" pattern="https?://.+" name="youtube" placeholder="http://www.youtube.com/..." value="{{old('youtube',$setting->youtube)}}" class="form-control">
            </div>
            <div class="form-group">
              <span><b>التخصص - عربي</b></span><br>
              <input type="text" name="ar_special" value="{{old('ar_special',$setting->ar_special)}}" class="form-control">
            </div>
            <div class="form-group">
              <span><b>التخصص - انجليزى </b></span><br>
              <input type="text" name="en_special" value="{{old('en_special',$setting->en_special)}}" class="form-control">
            </div>
            <div class="form-group">
              <span><b>الدرجه العلميه - عربي </b></span><br>
              <input type="text" name="ar_degree" value="{{old('ar_degree',$setting->ar_degree)}}" class="form-control" >
            </div>
            <div class="form-group">
              <span><b>الدرجه العلميه - انجليزى </b></span><br>
              <input type="text" name="en_degree" value="{{old('en_degree',$setting->en_degree)}}" class="form-control" >
            </div>
            <div class="form-group">
              <span><b>السيره الذاتيه - عربي </b></span><br>
              <textarea  name="ar_bio" class="form-control" >{{old('ar_bio',$setting->ar_bio)}}</textarea>
            </div>
            <div class="form-group">
              <span><b>السيره الذاتيه - انجليزى </b></span><br>
              <textarea  name="en_bio" class="form-control" >{{old('en_bio',$setting->en_bio)}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary" style="width:100%;">حفظ  </button>
            </div>
      </form>
    </div>
    </section>

@endsection
