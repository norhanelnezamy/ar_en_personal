@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboen_d</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">en_about</li>
      </ol>
    </section>
    <!-- Main content -->

    <section class="content">
    <div class="row col-md-8 well " style="margin:2%;">
      <form method="POST" action="{{ asset('/admin/setting/en_about') }}">
          {!! csrf_field() !!}
          <h3>عن</h3>

          <div class="form-group">
            <label>المحتوى الانجليزى </label>
            <textarea id="en_content" class="form-control" name="en_content" placeholder="enter content" required>{{old('en_content',$setting->en_about)}}</textarea>
            @if ($errors->has('en_content'))
             <p style="color:red">{{ $errors->first('en_content') }}</p>
            @endif
          </div>
          <button type="submit" class="btn btn-primary" style="width:100%;">حفظ</button>
      </form>
    </div>
    </section>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
    tinymce.init({
      selector: 'textarea',  // change this value according to your HTML
      images_upload_url: 'admin/upload'
    });
    </script>
  @endsection
