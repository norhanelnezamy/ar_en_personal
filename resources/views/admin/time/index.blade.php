@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">times</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" >
    <div class="row col-md-8 well " style="margin-left:1px;">
      <h4><a href="{{ asset('/admin/time/create') }}"> New time</a> </h4>
      <hr>
      <table class="table table-bordered">
          <thead>
          <tr>
              <th>#</th>
              <th>اليوم </th>
              <th>الميعاد </th>
              <th>التحكم </th>
          </tr>
          </thead>
          <tbody>
          @if(sizeof($times) > 0)
              <?php $count = 0?>
              @foreach($times as $time)
                  <?php $count++?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$time->day}}</td>
                    <td>{{$time->time}}</td>
                    <td>
                      <div class="form-inline">
                        <form action="{{ url('admin/time/'. $time->id.'/edit') }}" method="get">
                          <button class="glyphicon glyphicon-pencil" title="update"></button>
                        </form>
                        <form action="{{ url('admin/time/'. $time->id ) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field("DELETE") }}
                          <button class="glyphicon glyphicon-remove" title="delete"></button>
                        </form>
                      </div>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
      </table>
    </div>
    </section>
  @endsection
