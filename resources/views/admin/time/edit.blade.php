@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">add time</li>
      </ol>
    </section>
    <!-- Main content -->

    <section class="content">
    <div class="row col-md-8 well " style="margin:2%;">
      <form method="POST" action="{{ asset('/admin/time/'.$time->id) }}">
          {!! csrf_field() !!}
          {{method_field("PUT")}}
          <h3>Add time</h3>
          <hr>
          <div class="col-sm-6">
            <div class="form-group">
              <label></label>
              <input class="form-control" type="text" name="day" value="{{old('day',$time->day)}}" placeholder="enter day name" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label></label>
              <input class="form-control" type="text" name="time" value="{{old('time',$time->time)}}" placeholder="enter time" required>
            </div>
          </div>
          <button type="submit" class="btn btn-primary" style="width:100%;">تعديل </button>
      </form>
    </div>
    </section>
  @endsection
