@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">news</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" >
    <div class="row col-md-8 well " style="margin-left:1px;">
      <h4><a href="{{ asset('/admin/news/create') }}"> New news</a> </h4>
      <hr>
      <table class="table table-bordered">
          <thead>
          <tr>
              <th>#</th>
              <th>عنوان - عربي</th>
              <th>عنوان - انجليزى</th>
              <th>صوره </th>
              <th>التحكم </th>
          </tr>
          </thead>
          <tbody>
          @if(sizeof($news) > 0)
              <?php $count = 0?>
              @foreach($news as $one)
                  <?php $count++?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$one->ar_title}}</td>
                    <td>{{$one->en_title}}</td>
                    <td><img src="{{ asset('/images/'.$one->image) }}" style="width:70px; height:70px"></td>
                    <td>{{$one->created_at}}</td>
                    <td>
                      <div class="form-inline">
                        <form action="{{ url('admin/news/'. $one->id.'/edit') }}" method="get">
                          <button class="glyphicon glyphicon-pencil" title="update"></button>
                        </form>
                        <form action="{{ url('admin/news/'. $one->id ) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field("DELETE") }}
                          <button class="glyphicon glyphicon-remove" title="delete"></button>
                        </form>
                      </div>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
      </table>
      {{ $news->links() }}
    </div>
    </section>
  @endsection
