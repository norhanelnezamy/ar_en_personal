@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">articels</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" >
    <div class="row col-md-9 well " style="margin-left:1px;">
      <h4><a href="{{ asset('/admin/articel/create') }}"> اضافه مقال</a> </h4>
      <hr>
      <table class="table table-bordered">
          <thead>
          <tr>
              <th>#</th>
              <th>عنوان - عربي</th>
              <th>عنوان - انجليزى</th>
              <th>صوره </th>
              <th>التحكم </th>
          </tr>
          </thead>
          <tbody>
          @if(sizeof($articels) > 0)
              <?php $count = 0?>
              @foreach($articels as $articel)
                  <?php $count++?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$articel->ar_title}}</td>
                    <td>{{$articel->en_title}}</td>
                    <td><img src="{{ asset('/images/'.$articel->image) }}" style="width:70px; height:70px"></td>
                    <td>{{$articel->created_at}}</td>
                    <td>
                        <a class="btn btn-success glyphicon glyphicon-eye-open" title="show" href="{{asset('/admin/articel/'.$articel->id)}}"></a>
                        <form action="{{ url('admin/articel/'. $articel->id.'/edit') }}" method="get">
                          <button class="btn btn-warning glyphicon glyphicon-pencil" title="update"></button>
                        </form>
                        <form action="{{ url('admin/articel/'. $articel->id ) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field("DELETE") }}
                          <button class="btn btn-danger glyphicon glyphicon-remove" title="delete"></button>
                        </form>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
      </table>
      {{ $articels->links() }}
    </div>
    </section>
  @endsection
