@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">add case</li>
      </ol>
    </section>
    <!-- Main content -->

    <section class="content">
    <div class="row col-md-8 well " style="margin:2%;">
      <form method="POST" action="{{ asset('/admin/case') }}" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <h3>اضافه حالة </h3>
          <hr>
          <div class="col-sm-6 image">
            <div class="form-group">
              <span><b> صوره </b></span><br>
              <input type='file' name="image[]" id="image" multiple required/>
            </div>
          </div>
          <div class="col-sm-6 image">
            <div class="form-group">
              <img id="blah" src="http://placehold.it/150x150" alt="your image" style="width:150px;height:150px;"/>
            </div>
          </div>
          @if ($errors->has('image'))
           <p style="color:red">{{ $errors->first('image') }}</p>
          @endif
          <div class="form-group">
            <label>عنوان - عربي</label>
            <input class="form-control" type="text" name="ar_title" value="{{old('ar_title')}}" placeholder="enter arabic title">
            @if ($errors->has('ar_title'))
              <p style="color:red">{{ $errors->first('ar_title') }}</p>
            @endif
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>عنوان - انجليزى</label>
              <input class="form-control" type="text" name="en_title" value="{{old('en_title')}}" placeholder="enter english title" >
              @if ($errors->has('en_title'))
               <p style="color:red">{{ $errors->first('en_title') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label>المحتوى -عربي</label>
            <textarea id="ar_content" class="form-control" name="ar_content" placeholder="enter content" >{{old('ar_content')}}</textarea>
            @if ($errors->has('ar_content'))
             <p style="color:red">{{ $errors->first('ar_content') }}</p>
            @endif
          </div>
          <div class="form-group">
            <label>المحتوى - انجليزى </label>
            <textarea id="en_content" class="form-control" name="en_content" placeholder="enter content" >{{old('en_content')}}</textarea>
            @if ($errors->has('en_content'))
             <p style="color:red">{{ $errors->first('en_content') }}</p>
            @endif
          </div>
          <button type="submit" class="btn btn-primary" style="width:100%;">اضافه </button>
      </form>
    </div>
    </section>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
    tinymce.init({
      selector: 'textarea',  // change this value according to your HTML
      images_upload_url: 'admin/upload'
    });
    </script>
  @endsection
