@extends('layouts.admin')
  @section('content')
    <section class="content-header">
      <h1>
        Admin Panel
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{asset('/admin')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">cases</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" >
    <div class="row col-md-9 well " style="margin-left:1px;">
      <h4><a href="{{ asset('/admin/case/create') }}"> اضافه حالة</a> </h4>
      <hr>
      <table class="table table-bordered">
          <thead>
          <tr>
              <th>#</th>
              <th>عنوان - عربي</th>
              <th>عنوان - انجليزى</th>
              <th>صور </th>
              <th>التحكم </th>
          </tr>
          </thead>
          <tbody>
          @if(sizeof($cases) > 0)
              <?php $count = 0?>
              @foreach($cases as $case)
                  <?php $count++?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$case->ar_title}}</td>
                    <td>{{$case->en_title}}</td>
                    <td>
                      <?php $images = explode("|",$case->images); ?>
                      @foreach ($images as $image)
                        <img src="{{ asset('/images/'.$image) }}" style="width:70px; height:70px">
                      @endforeach
                    </td>
                    <td>{{$case->created_at}}</td>
                    <td>                        <form action="{{ url('admin/case/'. $case->id.'/edit') }}" method="get">
                          <button class="btn btn-warning glyphicon glyphicon-pencil" title="update"></button>
                        </form>
                        <form action="{{ url('admin/case/'. $case->id ) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field("DELETE") }}
                          <button class="btn btn-danger glyphicon glyphicon-remove" title="delete"></button>
                        </form>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
      </table>
      {{ $cases->links() }}
    </div>
    </section>
  @endsection
