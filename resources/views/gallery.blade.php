@extends('layouts.app')
  @section('content')
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-stellar-background-ratio="0.5" data-bg-img="{{asset('images/services-details.jpg')}}">
      <div class="container pt-100 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row">
            <div class="col-md-12">
              <h3 class="title text-white">معرض الصور </h3>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Gallery Grid 3 -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">

              <!-- Portfolio Gallery Grid -->
              <div id="grid" class="portfolio-gallery grid-4 gutter clearfix">
                @foreach ($gallery as $key => $item)
                  <!-- Portfolio Item Start -->
                  <div class="portfolio-item branding">
                    <div class="thumb">
                      <img class="img-fullwidth" src="{{asset('images/'.$item->image)}}" alt="project">
                      <div class="overlay-shade"></div>
                      <div class="icons-holder">
                        <div class="icons-holder-inner">
                          <div class="font-icons icon-sm icon-dark icon-circled icon-theme-colored">
                            {{$item->ar_title}}
                          </div>
                        </div>
                      </div>
                      <a class="hover-link" data-lightbox="image" href="{{asset('images/'.$item->image)}}">View more</a>
                    </div>
                  </div>
                  <!-- Portfolio Item End -->
                @endforeach
              </div>
              <!-- End Portfolio Gallery Grid -->

            </div>
          </div>
        </div>
      </div>
      {{$gallery->links()}}
    </section>
  @endsection
