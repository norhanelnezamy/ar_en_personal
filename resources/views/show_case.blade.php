@extends('layouts.app')
  @section('content')
    <div id="fb-root"></div>
<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/ar_AR/sdk.js#xfbml=1&version=v2.8&appId=327405644304210";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="blog-posts single-post">
              <article class="post clearfix mb-0">
                <div class="entry-header">
                  <div class="post-thumb thumb">
                    <?php $images = explode("|",$case->images); ?>
                    <img src="{{asset('images/'.$images[0])}}" alt="" class="img-responsive img-fullwidth">
                  </div>
                </div>
                <div id="grid" class="portfolio-gallery grid-4 gutter clearfix">
                  @foreach ($images as $image)
                    <div class="portfolio-item branding">
                      <div class="thumb">
                        <img class="img-fullwidth" src="{{asset('images/'.$image)}}" alt="project">
                        <div class="overlay-shade"></div>
                        <div class="icons-holder">
                          <div class="icons-holder-inner">
                            <div class="font-icons icon-sm icon-dark icon-circled icon-theme-colored">
                            </div>
                          </div>
                        </div>
                        <a class="hover-link" data-lightbox="image" href="{{asset('images/'.$image)}}"></a>
                      </div>
                    </div>
                  @endforeach
                </div>
                <div class="entry-content">
                  <div class="entry-meta media no-bg no-border mt-15 pb-20">
                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                      <ul>
                        <li class="font-12 text-uppercase">{{date_format($case->created_at ,'Y-m-d')}}</li>
                      </ul>
                    </div>
                    <div class="media-body pl-15">
                      <div class="event-content pull-left flip">
                        <h4 class="entry-title text-white text-uppercase m-0"><a href="#">{{$case->ar_title}}</a></h4>
                      </div>
                    </div>
                  </div>
                  <?php echo $case->ar_content;?>
                </div>
              </article>

              <div class="comments-area">
                <h5 class="comments-title">التعليقات </h5>
                <div class="fb-comments" data-href="{{asset('personal-site/case/'.$case->id)}}" data-numposts="1"></div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </section>
  @endsection
