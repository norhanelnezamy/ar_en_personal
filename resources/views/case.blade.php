@extends('layouts.app')
  @section('content')
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-stellar-background-ratio="0.5" data-bg-img="{{asset('images/services-details.jpg')}}">
      <div class="container pt-100 pb-50">
        <!-- Section Content -->
        <div class="section-content pt-100">
          <div class="row">
            <div class="col-md-12">
              <h3 class="title text-white">الحالات</h3>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row multi-row-clearfix">
          <div class="blog-posts">
            @foreach ($cases as $key => $item)
              <div class="col-md-3">
                <article class="post clearfix mb-30">
                  <div class="entry-header">
                    <div class="post-thumb thumb">
                      <?php $images = explode("|",$item->images); ?>
                      <img class="img-responsive img-fullwidth" alt="" src="{{asset('images/'.$images[0])}}">
                    </div>
                  </div>
                  <div class="entry-content p-20">
                    <h4 class="entry-title text-white text-uppercase"><a href="{{asset('case/'.$item->id)}}">{{$item->ar_title}}</a></h4>
                    <p><?php echo $item->ar_content;?> <a href="{{asset('case/'.$item->id)}}" class="text-theme-colored"><strong>Read More...</strong></a></p>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
            @endforeach
            <div class="col-md-12">
              <nav>
                <ul class="pagination theme-colored">
                  {{$cases->links()}}
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </section>
  @endsection
