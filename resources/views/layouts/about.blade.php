<!-- Section: About -->
<section id="about">
  <div class="container pt-80 pb-70 pb-sm-30 pb-xs-60">
    <div class="section-content">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 pb-sm-20 wow fadeInLeft animation-delay1">
          <h3 class="title font-weight-300 line-bottom">{{$setting->ar_name}}</h3>
          <p class="lead"><?php echo $setting->ar_about;?></p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 pb-sm-20 wow fadeInUp animation-delay3">
          <div class="image-box-thum">
            <img class="img-fullwidth" alt="" src="{{asset('images/'.$setting->image)}}"  style="width:360px;height:260px;">
          </div>
          <div class="image-box-details pt-20 pb-sm-20 text-center">
            <h4 class="title mt-0 font-weight-300">{{$setting->ar_name}}</h4>
            <p>{{$setting->ar_degree.' في '.$setting->ar_special}}</p>
            <p class="desc mb-10"><?php echo $setting->ar_bio;?></p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 pb-sm-20 wow fadeInRight animation-delay4">
          <div class="border-5px p-20">
            <h5 class="line-bottom mt-0"> ساعات العمل</h5>
            <div class="opening-hourse text-left flip">
              <ul class="list-unstyled">
                @foreach ($times as $key => $time)  
                  <li class="clearfix line-height-1"> <span><i class="fa fa-clock-o"></i> {{$time->day}}</span>
                    <div class="value"> {{$time->time}} </div>
                  </li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Section: Services -->
<section id="services" class="divider bg-lighter">
  <div class="container pt-50 pb-30">
    <div class="section-title text-center">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h2 class="title text-uppercase"><span class="text-black font-weight-300">خدماتنا</span></h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
        </div>
      </div>
    </div>
    <div class="section-content">
      <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="icon-box media p-20 m-0 text-center"> <a href="#" class="icon-border-effect effect-circled icon icon-circled ml-10 mr-10 mt-10"><i class="flaticon-medical-emergency text-tc"></i></a>
            <div class="media-body">
              <h5 class="media-heading heading font-16">Emergency Care</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam eveniet veritatis, iste maiores repudiandae recusandae.</p>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="icon-box media p-20 m-0 text-center"> <a href="#" class="icon-border-effect effect-circled icon icon-circled ml-10 mr-10 mt-10"><i class="flaticon-medical-operation4 text-tc"></i></a>
            <div class="media-body">
              <h5 class="media-heading heading font-16">Operation Thearer</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit incidunt cum eius consequatur enim maxime.</p>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="icon-box media p-20 m-0 text-center"> <a href="#" class="icon-border-effect effect-circled icon icon-circled ml-10 mr-10 mt-10"><i class="flaticon-medical-medical14 text-tc"></i></a>
            <div class="media-body">
              <h5 class="media-heading heading font-16">Outdoor Checkup</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum nulla magnam, facilis, dignissimos ratione asperiores!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
