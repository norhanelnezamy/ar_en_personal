<!DOCTYPE html>
<html dir="rtl" lang="en">
<head>

<!-- Meta Tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="MediC - Health & Medical HTML Template" />
<meta name="keywords" content="clinic,dentist,doctor,cleaning,hospital,workshop,business,medicine,service,surgeon,pharmacy,treatment,surgeon" />
<meta name="author" content="CodeClone" />
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', 'Laravel') }}</title>
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
<!-- Favicon and Touch Icons -->
<link href="{{asset('home/images/favicon.png')}}" rel="shortcut icon" type="image/png">
<link href="{{asset('home/images/apple-touch-icon.png')}}" rel="apple-touch-icon">
<link href="{{asset('home/images/apple-touch-icon-72.png')}}" rel="apple-touch-icon" sizes="72x72">
<link href="{{asset('home/images/apple-touch-icon-114.png')}}" rel="apple-touch-icon" sizes="114x114">
<link href="{{asset('home/images/apple-touch-icon-144.png')}}" rel="apple-touch-icon" sizes="144x144">

<link href="{{asset('home/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('home/css/style-main-rtl.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('home/css/style-main-rtl-extra.css')}}" rel="stylesheet" type="text/css">

<!-- Stylesheet -->
<link href="{{asset('home/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('home/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('home/css/animate.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('home/css/plugin-collections.css')}}" rel="stylesheet"/>
<!-- menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="{{asset('home/css/menuzord-skins/menuzord-subcolored.css')}}" rel="stylesheet"/>
<!-- Main style file -->
<link href="{{asset('home/css/style.css')}}" rel="stylesheet" type="text/css">
<!-- Theme Color -->
<link href="{{asset('home/css/colors/theme-skin-sky-blue.css')}}" rel="stylesheet" type="text/css">
<!-- Preloader Styles -->
<link href="{{asset('home/css/preloader.css')}}" rel="stylesheet" type="text/css">
<!-- Responsive Margin Padding Collection -->
<link href="{{asset('home/css/responsive-margin-padding.css')}}" rel="stylesheet" type="text/css">
<!--  Media queries for responsive -->
<link href="{{asset('home/css/responsive.css')}}" rel="stylesheet" type="text/css">
<!-- Client Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/client-style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="{{asset('home/js/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{asset('home/js/revolution-slider/css/layers.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{asset('home/js/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!-- [if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">

  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top sm-text-center bg-theme-colored">
      <div class="container">
        <div class="row">
          <div class="col-md-2 text-right flip sm-text-center">
            <div class="widget m-0">
              <ul class="font-icons icon-dark icon-circled icon-theme-colored icon-sm">
                <li class="mb-0 pb-0"><a href="{{$setting->facebook}}"><i class="fa fa-facebook"></i></a></li>
                <li class="mb-0 pb-0"><a href="{{$setting->twitter}}"><i class="fa fa-twitter"></i></a></li>
                <li class="mb-0 pb-0"><a href="{{$setting->youtube}}"><i class="fa fa-youtube"></i></a></li>
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-linkedin text-white"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-5">
            <div class="widget m-0 mt-5 no-border">
              <ul class="list-inline text-right sm-text-center">
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget">{{$setting->phone}} <i class="fa fa-phone"></i></div>
                </li>
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget">{{$setting->email}} <i class="fa fa-envelope-o"></i></div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-5">
            <nav>
              <ul class="list-inline sm-text-center text-left flip mt-5">
                <div> <a class="media-left pull-left flip" href="#"> <i class="pe-7s-map-2 text-tc"></i></a>
                    <p>{{$setting->ar_address}}</p>
                </div>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-lighter">
        <div class="container">
          <div class="row">
            <nav id="menuzord" class="menuzord blue bg-light">
              <input type="hidden" class="page" value="{{$page}}">
              <ul class="menuzord-menu">
                <li class="home"><a href="{{asset('/')}}">الرئيسيه </a></li>
                <li class="articel"><a href="{{asset('articel')}}">مقالات </a></li>
                <li class="gallery"><a href="{{asset('gallery')}}">معرض الصور </a></li>
                <li class="news"><a href="{{asset('news')}}">اخبار </a></li>
                <li class="cases"><a href="{{asset('case')}}">الحالات </a></li>
                <li class="about"><a href="{{asset('about')}}">عن {{$setting->ar_name}}</a></li>
                <li class="contact"><a href="{{asset('contact')}}">اتصل بنا </a></li>
             </ul>
           </nav>
         </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Start main-content -->
  <div class="main-content">
    @yield('slider')
    @yield('about')
    @yield('content')
  </div>
  <!-- main-content End -->

  <!-- Footer -->
  <footer id="footer" class="footer bg-black-222">
    <div class="container pb-50">
      <div class="row">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">الاتصال السريع </h5>
            <div class="widget p-0">
              <form id="quick_contact_form" name="quick_contact_form" class="quick-contact-form" action="includes/quickcontact.php" method="post">
                <div class="form-group">
                  <input id="form_email2" name="form_email2" class="form-control" type="text" required="" placeholder="Enter Email">
                </div>
                <div class="form-group">
                  <textarea id="form_message2" name="form_message2" class="form-control" required="" placeholder="Enter Message" rows="3"></textarea>
                </div>
                <div class="form-group">
                  <input id="form_botcheck2" name="form_botcheck2" class="form-control" type="hidden" value="" />
                  <button type="submit" class="btn btn-dark btn-tc btn-sm mt-0 text-black" data-loading-text="Please wait...">Send Message</button>
                </div>
              </form>
              <!-- Quick Contact Form Validation start-->
              <script type="text/javascript">
                $("#quick_contact_form").validate({
                  submitHandler: function(form) {
                    var form_btn = $(form).find('button[type="submit"]');
                    var form_result_div = '#form-result';
                    $(form_result_div).remove();
                    form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                    var form_btn_old_msg = form_btn.html();
                    form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                    $(form).ajaxSubmit({
                      dataType:  'json',
                      success: function(data) {
                        if( data.status == 'true' ) {
                          $(form).find('.form-control').val('');
                        }
                        form_btn.prop('disabled', false).html(form_btn_old_msg);
                        $(form_result_div).html(data.message).fadeIn('slow');
                        setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                      }
                    });
                  }
                });
              </script>
              <!-- Quick Contact Form Validation-->
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">اخري </h5>
            <ul class="list angle-double-right list-border">
              <li><a href="{{asset('news')}}">اخبار </a></li>
              <li><a href="{{asset('contact')}}">اتصل بنا </a></li>
              <li><a href="{{asset('about')}}">عن {{$setting->ar_name}} </a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-2">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">صفحات</h5>
            <ul class="list angle-double-right list-border">
              <li><a href="{{asset('articel')}}">مقالات </a></li>
              <li><a href="{{asset('gallery')}}">معرض الصور </a></li>
              <li><a href="{{asset('video')}}">فيديوهات </a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="widget dark">
            <img class="mt-10 mb-20" alt="" src="http://placehold.it/234x40">
            <div>
              <p>{{$setting->ar_degree.' في '.$setting->ar_special}}</p>
              <p class="desc mb-10"><?php echo $setting->ar_bio;?></p>
            </div>
            <ul class="list-inline mt-5">
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-tc mr-5"></i> <a class="text-gray" href="#">{{$setting->phone}}</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-tc mr-5"></i> <a class="text-gray" href="#">{{$setting->email}}</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-tc mr-5"></i> <a class="text-gray" href="#">www.yourdomain.com</a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid copy-right p-20 bg-black-333">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="font-11 text-black-777 m-0">Copyright &copy;2016 CodeClone. All Rights Reserved</p>
          <ul class="font-icons icon-bordered square list-inline mt-20">
            <li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube text-white"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram text-white"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin text-white"></i></a></li>
          </ul>
        </div>
        </div>
      </div>
    </div>
  </footer>

  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer script -->
<script src="{{asset('home/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('home/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('home/js/bootstrap.min.js')}}"></script>
<!-- plugin collection -->
<script src="{{asset('home/js/plugin-collection.js')}}"></script>
<!-- Revolution Slider 5.x SCRIPTS -->
<script src="{{asset('home/js/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('home/js/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>
<!-- Google Map Javascript Codes -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyABqK-5ngi3F1hrEsk7-mCcBPsjHM5_Gj0"></script>
<script src="{{asset('home/js/google-map-init.js')}}"></script>

<!-- Custom script for all pages -->
<script src="{{asset('home/js/custom.js')}}"></script>
<script>
  $(document).ready(function() {
    var page = $('input.page').val();
    $('li.'+page).addClass('active');
  });
</script>
<script>
  $(document).ready(function(e) {
    $(".rev_slider").revolution({
      sliderType: "standard",
      sliderLayout:"fullscreen",
      dottedOverlay:"none",
      delay:5000,
      navigation: {
        keyboardNavigation:"off",
        keyboard_direction: "horizontal",
        mouseScrollNavigation:"off",
        onHoverStop:"off",
        touch:{
            touchenabled:"on",
            swipe_threshold: 75,
            swipe_min_touches: 1,
            swipe_direction: "horizontal",
            drag_block_vertical: false
        }
        ,
        bullets: {
            enable:true,
            hide_onmobile:true,
            style:"zeus",
            hide_onleave:true,
            direction:"vertical",
            h_align:"bottom",
            v_align:"center",
            h_offset:30,
            v_offset:0,
            space:10,
            tmp:''
        }
        ,
        thumbnails: {
          style:"gyges",
          enable:true,
          width:60,
          height:60,
          min_width:60,
          wrapper_padding:0,
          wrapper_color:"#000000",
          wrapper_opacity:"0",
          tmp:'<span class="tp-thumb-img-wrap">  <span class="tp-thumb-image"></span></span>',
          visibleAmount:10,
          hide_onmobile:true,
          hide_onleave:true,
          direction:"horizontal",
          span:false,
          position:"inner",
          space:10,
          h_align:"left",
          v_align:"bottom",
          h_offset:30,
          v_offset:30
        }
      },
      responsiveLevels:[1240,1024,778,480],
      gridwidth: [1170, 1024, 778, 480],
      gridheight: [600, 768, 960, 720],
      lazyType:"none",
      parallax: {
          origo: "slidercenter",
          speed: 1000,
          levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
          type: "scroll"
      },
      shadow:0,
      spinner:"off",
      stopLoop:"on",
      stopAfterLoops:0,
      stopAtSlide: -1,
      shuffle:"off",
      autoHeight:"off",
      hideThumbsOnMobile:"off",
      hideSliderAtLimit:0,
      hideCaptionAtLimit:0,
      hideAllCaptionAtLilmit:0,
      debugMode:false,
      fallbacks: {
        simplifyAll:"off",
        nextSlideOnWindowFocus:"off",
        disableFocusListener:false
      }
    });
  });
</script>
<!-- Slider Revolution Ends -->

<!-- Contact Form Validation-->
<script type="text/javascript">
  $("#contact_form").validate({
    submitHandler: function(form) {
      var form_btn = $(form).find('button[type="submit"]');
      var form_result_div = '#form-result';
      $(form_result_div).remove();
      form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
      var form_btn_old_msg = form_btn.html();
      form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
      $(form).ajaxSubmit({
        dataType:  'json',
        success: function(data) {
          if( data.status == 'true' ) {
            $(form).find('.form-control').val('');
          }
          form_btn.prop('disabled', false).html(form_btn_old_msg);
          $(form_result_div).html(data.message).fadeIn('slow');
          setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
        }
      });
    }
  });
</script>
</body>
</html>
