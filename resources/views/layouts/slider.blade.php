
    <!-- Section: home -->
    <section id="home">
      <div class="container-fluid p-0">

        <!-- Slider Revolution Start -->
        <div class="rev_slider_wrapper">
          <div class="rev_slider" data-version="5.0">
            <ul>
              @foreach ($slider as $key => $item)
                <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{asset('images/'.$item->image)}}" data-rotate="0" data-saveperformance="off" data-title="Wow Factor" data-description="">
                  <!-- MAIN IMAGE -->
                  <img src="{{asset('images/'.$item->image)}}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
                  <!-- LAYERS -->

                  <!-- LAYER NR. 1 -->
                  <div class="tp-caption NotGeneric-Title tp-resizeme text-uppercase"
                    id="rs-1-layer-1"
                    data-x="['left','left','left','left']"
                    data-hoffset="['50','50','50','50']"
                    data-y="['top','top','top','top']"
                    data-voffset="['150','150','170','168']"
                    data-fontsize="['72','72','56','32']"
                    data-lineheight="['100','90','60','60']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="x:-50px;opacity:0;s:500;e:Power1.easeInOut;"
                    data-transform_out="x:50px;opacity:0;s:500;e:Power1.easeInOut;"
                    data-start="500"
                    data-splitin="chars"
                    data-splitout="none"
                    data-basealign="slide"
                    data-responsive_offset="on"
                    data-elementdelay="0.03"
                    style="z-index: 5; white-space: nowrap; font-weight:700;">{{$item->ar_title}} <span class="text-tc">Solution</span>
                  </div>

                  <!-- LAYER NR. 2 -->
                  <div class="tp-caption NotGeneric-SubTitle tp-resizeme text-uppercase text-white"
                    id="rs-1-layer-2"
                    data-x="['left','left','left','left']"
                    data-hoffset="['55','55','55','55']"
                    data-y="['top','top','top','top']"
                    data-voffset="['160','160','160','160']"
                    data-fontsize="['18','18','18','18']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="x:-50px;opacity:0;s:500;e:Power1.easeInOut;"
                    data-transform_out="x:50px;opacity:0;s:500;e:Power1.easeInOut;"
                    data-start="500"
                    data-splitin="chars"
                    data-splitout="none"
                    data-basealign="slide"
                    data-responsive_offset="on"
                    data-elementdelay="0.03"
                    style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 0.50);">
                  </div>

                  <!-- LAYER NR. 3 -->
                  <div class="tp-caption Photography-Textblock tp-resizeme"
                    id="rs-1-layer-3"
                    data-x="['left','left','left','left']"
                    data-hoffset="['55','55','55','55']"
                    data-y="['top','top','top','top']"
                    data-voffset="['250','250','250','250']"
                    data-width="380"
                    data-height="150"
                    data-whitespace="normal"
                    data-transform_idle="o:1;"
                    data-transform_in="x:-50px;opacity:0;s:500;e:Power1.easeInOut;"
                    data-transform_out="x:50px;opacity:0;s:500;e:Power1.easeInOut;"
                    data-start="500"
                    data-splitin="chars"
                    data-splitout="none"
                    data-basealign="slide"
                    data-responsive_offset="on"
                    data-elementdelay="0.01"
                    style="z-index: 7; min-width: 380px; max-width: 380px; max-width: 180px; max-width: 180px; white-space: normal; font-size: 15px; line-height: 25px;">{{$item->ar_text}}
                  </div>

                  <!-- LAYER NR. 4 -->
                  <div class="tp-caption BigBold-Button rev-btn text-uppercase"
                    id="rs-1-layer-4"
                    data-x="['left']"
                    data-hoffset="['50']"
                    data-y="['top']"
                    data-voffset="['350']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="x:-50px;opacity:0;s:500;e:Power1.easeInOut;"
                    data-transform_out="x:50px;opacity:0;s:500;e:Power1.easeInOut;"
                    data-start="1000"
                    data-splitin="none"
                    data-splitout="none"
                    data-basealign="slide"
                    data-responsive_offset="on"
                    data-responsive="on"
                    style="z-index: 8; white-space: nowrap;text-transform:left;border-color:rgba(255, 255, 255, 0.25);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">About more <i style="font-size: .8rem; vertical-align: middle;" class="fa fa-arrow-circle-right text-white ml-5"></i>
                 </div>
                </li>
              @endforeach
            </ul>
          </div>
          <!-- end .rev_slider -->
        </div>
        <!-- end .rev_slider_wrapper -->

      </div>
    </section>
